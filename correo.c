#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include "util.h"
#include "jsocket.h"
#include "casilla.h"

// Typedefs utiles
typedef void *(*Thread_fun)(void *);

// Variables globales
Casilla d; // casilla de correo
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t c = PTHREAD_COND_INITIALIZER;

void *servidor(long s) {
  int s2 = (int)s; // s2 es el FD a usar

  char *input = getstr(s2); // input contiene a la opcion
  switch(input[0])
    {
    case 'e':
      // Almacenamiento de mensaje en la casilla

      // Liberar espacio de la opcion
      free(input);

      // Enviar respuesta al cliente para que no se preocupe
      sendstr(s2, "optok");
      if ((input = getstr(s2)) == NULL) {
	fprintf(stderr, "Error: no se recibio correctamente la prioridad.\n");
      }

      // Obtener prioridad de forma numerica
      int pri = atoi(input);

      // Enviar respuesta al cliente para que no se preocupe
      sendstr(s2, "priok");

      // Liberamos string con la prioridad
      free(input);

      // Obtener mensaje como tal
      if ((input = getstr(s2)) == NULL) {
	fprintf(stderr, "Error: no se recibio correctamente el texto.\n");
      }

      // Enviar respuesta al cliente para que no se preocupe
      sendstr(s2, "msgok");
      
      // Añadimos en la lista de prioridad el mensaje
      pthread_mutex_lock(&m);
      enviar(d, input, pri);
      pthread_cond_broadcast(&c);
      pthread_mutex_unlock(&m);

      // Liberamos mensaje como tal
      free(input);

      // Listo
      break;
    case 'r':
      // Recepcion de mensaje en el cliente (envio desde la casilla)

      // Liberamos espacio de la opcion
      free(input);

      // Recibimos el string de la casilla. Ojo que es un puntero a un espacio de memoria global (no se libera)
      char *output = recibir(d);

      // Se envia el mensaje al cliente
      sendstr(s2,output);

      // Listo
      break;
    default:
      fprintf(stderr, "Error: mensaje desde el cliente no reconocido");	    
    }
  
  // Se cierra conexion
  close(s2);
  
  // Retorna el thread
  return NULL;
}

int main(int argc, char **argv) {
  // Chequeo de inexistencia de argumentos
  if (argc != 1) fprintf(stderr, "Error: el programa no recibe argumentos.\n");

  // FD del socket a aceptar
  long s, s2;

  // Declaramos thread
  pthread_t pid;

  // Creamos casilla a partir de espacio global de memoria
  d = nuevaCasilla();

  // Evitamos que se cierre el programa por un quiebre del socket
  signal(SIGPIPE, SIG_IGN);

  // Creamos socket
  s = j_socket();
   
  // Intentamos conexion al socket
  if(j_bind(s, 3002) < 0) {
    fprintf(stderr, "Error: probablemente el port 3002 ya se encuentra tomado.\n");
    exit(1);
  }

  // Un thread por cliente
  for(;;) {
    pthread_attr_t attr;
    s2 = j_accept(s);
    pthread_attr_init(&attr);
    if(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0) {
      fprintf(stderr, "Error: atributo del thread no se pudo establecer.\n");
    }
    if (pthread_create(&pid, &attr, (Thread_fun)servidor, (void *)s2) != 0) {
      fprintf(stderr, "Error: no pude crear thread para nuevo cliente.\n");
      close(s2);
    }
    pthread_attr_destroy(&attr);
  }
  
  return 0;
}
