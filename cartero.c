#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "jsocket.h"

int main(int argc, char **argv) {
  // Manejo de argumentos
  if (argc < 2) {
    fprintf(stderr,"Error: Debe especificar una opción: r o e.\n");
    return 1;
  }

  // Parametros del socket
  char *host = "localhost";
  int port = 3002;
  int s;
  if (strcmp(argv[1],"r") == 0) {

    // Mas manejo de argumentos pero para la opcion 'r'
    if (argc != 2) {
      fprintf(stderr,"Error: Número de argumentos incorrecto.\n");
      exit(1);
    }
    
    // Conectar con servidor
    s = j_socket();
    if (j_connect(s, host, port) < 0) {
      fprintf(stderr, "Error: Conexion a servidor fallida.\n");
      exit(1);
    }

    // Envio de instruccion de recepcion
    sendstr(s, "r");

    // Recepcion de respuesta
    char *res = getstr(s);

    // Imprimir en pantalla mensaje
    printf("%s\n",res);

    // Liberamos memoria
    free(res);

    // Nada mas que hacer
    return 0;
  }

  if (strcmp(argv[1],"e") == 0) {
    // Mas manejo de memoria, pero para la opcion 'e'
    if (argc != 4) {
      fprintf(stderr,"Error: Numero de argumentos incorrecto.\n");
      exit(1);
    }

    // Conectar con servidor
    s = j_socket();
    if (j_connect(s, host, port) < 0) {
      fprintf(stderr, "Error: Conexion a servidor fallida.\n");
      exit(1);
    }
    
    // Envio de instruccion de envio
    sendstr(s, "e");

    // Respuesta del servidor
    if (strcmp(getstr(s),"optok") != 0) {
      fprintf(stderr, "Error: mensaje recibido no es el esperado (opcion).\n");
      exit(1);
    }

    // Envio de prioridad
    sendstr(s, argv[2]);

    // Respuesta del servidor
    if (strcmp(getstr(s),"priok") != 0) {
      fprintf(stderr, "Error: mensaje recibido no es el esperado (prioridad).\n");
      exit(1);
    }

    // Envio de mensaje en texto
    sendstr(s, argv[3]);

    if (strcmp(getstr(s),"msgok") != 0) {
      fprintf(stderr, "Error: mensaje recibido no es el esperado (texto).\n");
      exit(1);
    }

    // Se cierra conexion con servidor
    close(s);

    // Nada mas que hacer
    return 0;
  }
  
  // Si no se especifico 'e' ni 'r', salir
  fprintf(stderr,"Error: no se especifico una opcion valida.\n");
  exit(1);
}  
