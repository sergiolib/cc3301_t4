#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdbool.h>

#include "colapri.h"
#include "colapri-arreglo.h"

/* Mensaje */
typedef struct {
  char *m; // String con el mensaje
  int pri; // Prioridad
  bool recieved; // Recibido?
} Msg;

/* Casilla */
typedef struct {
  ColaPri msgl; // Cola de prioridad
  pthread_mutex_t m; // monitor
  pthread_cond_t c;
} *Casilla;

/* Funciones implementadas */
Casilla nuevaCasilla();
void enviar(Casilla c, void *msg, int pri);
void *recibir(Casilla c);

/* Esta funcion esta implementada en el archivo test-casilla.c */
void error(char *fmt, ...);
