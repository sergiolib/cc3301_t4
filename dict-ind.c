#include <stdio.h>
#include <stdlib.h>
#include "dict.h"
#include "dict-ind.h"

typedef struct nodo {
  uchar i;
  char *valor;
  struct nodo *prox;
} Nodo;

typedef struct {
  DictOps *ops;
  Comparador comp;
  int n;
  Nodo **nodos;
} DictInd;

void agregarDictInd(Dict d, uchar k, char *v);
int vacioDictInd(Dict d);
char *obtenerDictInd(Dict d, uchar k);
void destruirDictInd(Dict d);

static DictOps dictIndOps= { agregarDictInd, vacioDictInd, obtenerDictInd, destruirDictInd };

Dict nuevoDictInd(int n) {
  DictInd *d= (DictInd*)malloc(sizeof(DictInd));
  int i;
  d->ops= &dictIndOps;
  d->n= n;
  d->nodos= (Nodo**)malloc(n * sizeof(Nodo*));
  for (i= 0; i<n; i++){
    ((d->nodos)[i])=NULL;
  }
  return (Dict)d;
}

int vacioDictInd(Dict d) {
  DictInd *da= (DictInd*)d;
  int i;
  for (i= 0; i<da->n; i++){
    if ((da->nodos)[i]!=NULL)
      return 0;
  }
  return 1;
}

void agregarDictInd(Dict d, uchar k, char *v) {
  DictInd *da= (DictInd*)d;
  Nodo *pnodoPrev= (da->nodos)[(int)k];
  Nodo *pnodo= (Nodo*)malloc(sizeof(Nodo));
  pnodo->i= k;
  strcpy(pnodo->valor,v);
  pnodo->prox= pnodoPrev;    
  da->nodos[(int)k]= pnodo;
}

char *obtenerDictInd(Dict d, uchar k) {
  DictInd *da= (DictInd*)d;
  char *r;
  if (da->nodos[(int)k] != NULL) {
    r = malloc((1+strlen(da->nodos[(ind)k]->valor))*sizeof(char));
    strcpy(r,da->nodos[(int)k]->valor);
    if (da->nodos[(int)k]->prox != NULL) {
      da->nodos[(int)k]->valor = da->nodos[(int)k]->prox->valor;
      Nodo *aux = da->nodos[(int)k]->prox->prox;
      free(da->nodos[(int)k]->prox);
      da->nodos[(int)k]->prox = aux;
      return r;
    }
    free(da->nodos[(int)k]);
    da->nodos[(int)k] = NULL;
    return r;
  }
  return NULL;
}

void destruirDictInd(Dict d) {
  DictHash *da= (DictHash*)d;
  int i;
  for (i = 0; i<da->n; i++) {
    while (da->nodos[i] != NULL) {
      Nodos *aux = da->nodos[i]->prox;
      free(da->nodos[i]);
      da->nodos[i] = aux;
    }
    free(da->nodos);
  }
  Free(da);
}

