#include "casilla.h"

/* Definiciones estaticas */
#define M 10 // Largo de cola de prioridad
static int msgcmp(Msg *a, Msg *b); // Funcion de comparacion de mensajes

/* Funciones implementadas */
Casilla nuevaCasilla() {
  // instancio casilla
  Casilla n = malloc(sizeof(*n));
  if (n == NULL) {
    printf("No hay memoria!\n");
  }

  // cola de prioridad
  n->msgl = nuevaColaPriArreglo(M, (CompPri)msgcmp);

  // monitor
  pthread_mutex_init(&n->m,NULL);
  pthread_cond_init(&n->c,NULL);
  
  return n;
}

void enviar(Casilla c, void *msg, int pri) {
  pthread_mutex_lock(&c->m);

  // Cola llena. Espero a que se extraigan mensajes
  while (c->msgl->ops->tamano(c->msgl) == M)
    pthread_cond_wait(&c->c, &c->m);

  // Creo nuevo mensaje
  Msg *nuevo = malloc(sizeof(nuevo));
  if (nuevo == NULL) {
    printf("No hay memoria!\n");
  }
  nuevo->m = msg; 
  nuevo->pri = pri;
  nuevo->recieved = false;

  // Envio mensaje
  c->msgl->ops->agregar(c->msgl,nuevo);

  // Le aviso al mundo
  pthread_cond_broadcast(&c->c);

  // Espero a que el mensaje sea recibido
  while (nuevo->recieved == false) {
    pthread_cond_wait(&c->c, &c->m);
  }
  
  // Elimino el mensaje
  free(nuevo);

  // Desbloqueo memoria
  pthread_cond_broadcast(&c->c);
  pthread_mutex_unlock(&c->m);
}

void *recibir(Casilla c) {
  pthread_mutex_lock(&c->m);

  // si no hay mensajes en la cola, espero
  while (c->msgl->ops->tamano(c->msgl) == 0)
    pthread_cond_wait(&c->c, &c->m);

  // extraigo el mensaje de mayor prioridad
  Msg *m = c->msgl->ops->extraer(c->msgl);
  char *msg = m->m;

  // aviso que el mensaje se ha recibido
  m->recieved = true;
  pthread_cond_broadcast(&c->c);

  // no uso mas la memoria
  pthread_mutex_unlock(&c->m);
  
  // retorno mensaje
  return msg;
}

/* Funcion comparadora de mensajes */
static int msgcmp(Msg *a, Msg *b) {
  if (a->pri < b->pri)
    return 1;
  if (a->pri > b->pri)
    return -1;
  return 0;
}
