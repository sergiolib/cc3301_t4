#include <stdbool.h>
/* Diccionario generico */

typedef unsigned char uchar;
typedef struct dict *Dict;

/* Tabla de operaciones para un diccionario */
typedef struct {
  void (*agregar)(Dict d, uchar k, char *m);
  bool (*vacio)(Dict d);
  char *(*obtener)(Dict d, uchar k);
  void (*destruir)(Dict d);
} DictOps;

/* El �nico campo visible de un diccionario es su tabla de operaciones */
struct dict {
  DictOps *ops;
};
