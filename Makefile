# Defina aca la ubicacion de jsockets4
LIBJSOCKET=jsockets4

CFLAGS= -g -Wall -pedantic -std=c99 -I$(LIBJSOCKET)
LDFLAGS= -g -pthread

all: correo cartero

colapri-arreglo.o: colapri.h colapri-arreglo.h

casilla.o: casilla.h colapri-arreglo.h colapri.h

correo.o: util.h casilla.h

cartero.o: util.h casilla.h

correo: correo.o casilla.o util.o colapri-arreglo.o $(LIBJSOCKET)/libjsocket.o

cartero: cartero.o util.o $(LIBJSOCKET)/libjsocket.o

clean:
	rm -f *.o correo cartero
