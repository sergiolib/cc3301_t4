#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "jsocket.h"

/*
 * server_echo2: servidor de echo multi-cliente usando procesos pesados
 * si un hijo muere, no importa
 * si mato con ctrl-C se mueren todos: para independizarse del terminal
 * debieramos usar setpgrp()
 */

#define BUF_SIZE 200

/*
 * Esta es la forma mas simple de enterrar a los hijos sin complicarse la vida
 */
void child() {
    int status;

    while(waitpid(-1, &status, WNOHANG)>0)
		;
}

/*
 * Este es el servidor y el codigo para un socket cliente ya conectado: s
 */
void serv(int s) {
   int cnt, size = BUF_SIZE;
   char buf[BUF_SIZE];

    fprintf(stderr, "cliente conectado\n");
    while((cnt=read(s, buf, size)) > 0)
	 write(s, buf, cnt);
    fprintf(stderr, "cliente desconectado\n");
}

/*
 * Este es el principal: solo acepta conexiones y crea a los hijos servidores
 */
int main() {
   int s, s2;

   signal(SIGCHLD, child);

   s = j_socket();

   if(j_bind(s, 1818) < 0) {
	fprintf(stderr, "bind failed\n");
	exit(1);
   }

   for(;;) {
	s2 = j_accept(s);
	if(fork() == 0) { /* Este es el hijo */
	    close(s); /* cerrar el socket que no voy a usar */
	    serv(s2);
	    exit(0);
	} else
	    close(s2); /* cerrar el socket que no voy a usar */
   }

   return 0;
}

