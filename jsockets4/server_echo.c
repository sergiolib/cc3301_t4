#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "jsocket.h"

/* 
 * server_echo: servidor de echo mono-cliente
 */

#define BUF_SIZE 200

int main() {
   int s, s2;
   int cnt, size = BUF_SIZE;
   char buf[BUF_SIZE];

   s = j_socket();

   if(j_bind(s, 1818) < 0) {
	fprintf(stderr, "bind failed\n");
	exit(1);
   }

   for(;;) {
	s2 = j_accept(s);
	fprintf(stderr, "cliente conectado: %d\n", s2);
        while((cnt=read(s2, buf, size)) > 0)
	    write(s2, buf, cnt);
        close(s2);
	fprintf(stderr, "cliente desconectado\n");
   }

   return 0;
}
