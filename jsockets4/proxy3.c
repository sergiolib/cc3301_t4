#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>
#include <unistd.h>
#include "jsocket.h"

/*
 * proxy: proxy multi-cliente usando procesos pesados
 * version con threads
 */

#define BUF_SIZE 200

/*
 * Esta es la forma mas simple de enterrar a los hijos sin complicarse la vida
 */
void child() {
    int status;

    while(waitpid(-1, &status, WNOHANG)>0)
		;
}

void *copy(void *fdsp) {
    int cnt, size = BUF_SIZE;
    char buf[BUF_SIZE];
    int *fds = fdsp;

   
    while((cnt=read(fds[0], buf, size)) > 0)
        write(fds[1], buf, cnt);
    
    fprintf(stderr, "cliente desconectado\n");
    exit(0);
}

/*
 * Este es el servidor y el codigo para un socket cliente ya conectado: s
 */
void proxy(int s1, char *host, int portout) {
   int s2;
   pthread_t pid;
   int fds1[2], fds2[2];

   s2 = j_socket();

   if(j_connect(s2, host, portout) < 0) {
	fprintf(stderr, "connection refused to %s/%d\n", host, portout);
	exit(1);
   }
   fprintf(stderr, "cliente conectado\n");

   fds1[0] = s1; fds1[1] = s2;
   if(pthread_create(&pid, NULL, copy, (void *)fds1) < 0) {
	fprintf(stderr, "No pude crear thread!\n");
	exit(1);
   }
   fds2[0] = s2; fds2[1] = s1;
   copy(fds2);
   exit(0);
}

/*
 * Este es el principal: solo acepta conexiones y crea a los hijos servidores
 */
int main(int argc, char **argv) {
   int s, s2;
   int portin, portout;
   char *host;

   if(argc != 4) {
	fprintf(stderr, "Use: %s port-in host port-out\n", argv[0]);
	exit(1);
   }

   portin = atoi(argv[1]);
   host = argv[2];
   portout = atoi(argv[3]);

   signal(SIGCHLD, child);

   s = j_socket();

   if(j_bind(s, portin) < 0) {
	fprintf(stderr, "bind failed\n");
	exit(1);
   }

   for(;;) {
	s2 = j_accept(s);
	if(fork() == 0) { /* Este es el hijo */
	    close(s); /* cerrar el socket que no voy a usar */
	    proxy(s2, host, portout);
	    exit(0);
	} else
	    close(s2); /* cerrar el socket que no voy a usar */
   }
}

