#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "jsocket.h"

#define BUF_SIZE 1024
 
int main() {
   int s;
   int cnt, cnt2;
   char buf[BUF_SIZE];
 
   s = j_socket();
 
   if(j_connect(s, "localhost", 1818) < 0) {
	fprintf(stderr, "connection refused\n");
	exit(1);
   }
 
   while((cnt=read(0, buf, BUF_SIZE)) > 0) {
	if(write(s, buf, cnt) != cnt) {
	   fprintf(stderr, "Fall el write al servidor\n");
	   exit(1);
	}
	while(cnt > 0 && (cnt2=read(s, buf, BUF_SIZE)) > 0) {
	   write(1, buf, cnt2);
	   cnt -= cnt2;
	}
   }
 
   return 0;
}
