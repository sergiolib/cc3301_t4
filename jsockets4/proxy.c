#define _POSIX_C_SOURCE 1
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "jsocket.h"

/*
 * proxy: proxy multi-cliente usando procesos pesados
 * Hacer version con select?
 */

#define BUF_SIZE 200

/*
 * Esta es la forma mas simple de enterrar a los hijos sin complicarse la vida
 */
void child() {
    int status;

    while(waitpid(-1, &status, WNOHANG)>0)
		;
}

void die() {
    fprintf(stderr, "cliente desconectado\n");
    exit(0);
}

/*
 * Este es el servidor y el codigo para un socket cliente ya conectado: s
 */
void proxy(int s1, char *host, int portout) {
   int cnt, size = BUF_SIZE;
   char buf[BUF_SIZE];
   int s2;
   int pid;

   signal(SIGUSR1, die);

   s2 = j_socket();

   if(j_connect(s2, host, portout) < 0) {
	fprintf(stderr, "connection refused to %s/%d\n", host, portout);
	exit(1);
   }
   fprintf(stderr, "cliente conectado\n");
	
   if((pid = fork()) < 0) {
    	fprintf(stderr, "no pude fork\n");
	exit(1);
   }

   if(pid == 0) {
    	while((cnt=read(s1, buf, size)) > 0)
	    if(write(s2, buf, cnt) < 0) break;
	kill(getppid(), SIGUSR1);
	exit(0);
   } else
    	while((cnt=read(s2, buf, size)) > 0)
	    if(write(s1, buf, cnt) < 0) break;

    kill(pid, SIGKILL);
    fprintf(stderr, "cliente desconectado\n");
}

/*
 * Este es el principal: solo acepta conexiones y crea a los hijos servidores
 */
int main(int argc, char **argv) {
   int s, s2;
   int portin, portout;
   char *host;

   if(argc != 4) {
	fprintf(stderr, "Use: %s port-in host port-out\n", argv[0]);
	exit(1);
   }

   portin = atoi(argv[1]);
   host = argv[2];
   portout = atoi(argv[3]);

   signal(SIGCHLD, child);

   s = j_socket();

   if(j_bind(s, portin) < 0) {
	fprintf(stderr, "bind failed\n");
	exit(1);
   }

   for(;;) {
	s2 = j_accept(s);
	if(fork() == 0) { /* Este es el hijo */
	    close(s); /* cerrar el socket que no voy a usar */
	    proxy(s2, host, portout);
	    exit(0);
	} else
	    close(s2); /* cerrar el socket que no voy a usar */
   }
}
