#define _BSD_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/select.h>
#include "jsocket.h"

/*
 * proxy: proxy multi-cliente usando procesos pesados
 * version con select
 */

#define BUF_SIZE 200

/*
 * Esta es la forma mas simple de enterrar a los hijos sin complicarse la vida
 */
void child() {
    int status;

    while(waitpid(-1, &status, WNOHANG)>0)
		;
}

/*
 * Este es el servidor y el codigo para un socket cliente ya conectado: s
 */
void proxy(int s1, char *host, int portout) {
   int cnt, size = BUF_SIZE;
   char buf[BUF_SIZE];
   int s2;
   fd_set mask;

   s2 = j_socket();

   if(j_connect(s2, host, portout) < 0) {
	fprintf(stderr, "connection refused to %s/%d\n", host, portout);
	exit(1);
   }
   fprintf(stderr, "cliente conectado\n");
	
/*
       int select(int numfds, fd_set *readfds, fd_set *writefds ,
       fd_set *exceptfds, struct timeval * timeout);

       FD_CLR(int fd, fd_set *set);
       FD_ISSET(int fd, fd_set *set);
       FD_SET(int fd, fd_set *set);
       FD_ZERO(fd_set * set);
*/
   
   
    for(;;) {
        FD_ZERO(&mask);
        FD_SET(s1, &mask); FD_SET(s2, &mask);
	select(getdtablesize(), &mask, NULL, NULL, NULL);
	if(FD_ISSET(s1, &mask)) {
    	    cnt=read(s1, buf, size);
	    if(cnt <= 0) {
    		fprintf(stderr, "cliente desconectado\n");
		exit(0);
	    }
	    write(s2, buf, cnt);
	}
	if(FD_ISSET(s2, &mask)) {
    	    cnt=read(s2, buf, size);
	    if(cnt <= 0) {
    		fprintf(stderr, "cliente desconectado\n");
 		exit(0);
	    }
	    write(s1, buf, cnt);
	}
    }
}

/*
 * Este es el principal: solo acepta conexiones y crea a los hijos servidores
 */
int main(int argc, char **argv) {
   int s, s2;
   int portin, portout;
   char *host;

   if(argc != 4) {
	fprintf(stderr, "Use: %s port-in host port-out\n", argv[0]);
	exit(1);
   }

   portin = atoi(argv[1]);
   host = argv[2];
   portout = atoi(argv[3]);

   signal(SIGCHLD, child);

   s = j_socket();

   if(j_bind(s, portin) < 0) {
	fprintf(stderr, "bind failed\n");
	exit(1);
   }

   for(;;) {
	s2 = j_accept(s);
	if(fork() == 0) { /* Este es el hijo */
	    close(s); /* cerrar el socket que no voy a usar */
	    proxy(s2, host, portout);
	    exit(0);
	} else
	    close(s2); /* cerrar el socket que no voy a usar */
   }
}

