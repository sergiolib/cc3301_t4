#define _BSD_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include "jsocket.h"

/*
 * server_echo3: servidor multi-clientes usando select
 */

/*
       int select(int numfds, fd_set *readfds, fd_set *writefds ,
       fd_set *exceptfds, struct timeval * timeout);

       FD_CLR(int fd, fd_set *set);
       FD_ISSET(int fd, fd_set *set);
       FD_SET(int fd, fd_set *set);
       FD_ZERO(fd_set * set);
*/

#define BUF_SIZE 200
#define MAX_CLIENTS 2

int main() {
   int s;
   int cnt, size = BUF_SIZE;
   char buf[BUF_SIZE];
   int ret;
   fd_set mask;
   int client[MAX_CLIENTS];
   int i;

/* para no morirme al morir un cliente */
   signal(SIGPIPE, SIG_IGN);

/* Arreglo de sockets de los clientes */
   for( i = 0; i < MAX_CLIENTS; i++ )
	client[i] = -1;

   s = j_socket();

   if(j_bind(s, 1818) < 0) {
	fprintf(stderr, "bind failed\n");
	exit(1);
   }

   for(;;) {
	FD_ZERO(&mask);
	FD_SET(s, &mask);	/* el socket de accept */
	for(i = 0; i < MAX_CLIENTS; i++) /* cada socket cliente */
		if( client[i] != -1 )
		    FD_SET(client[i], &mask);

/* espero que haya actividad en algun socket */
	ret = select( getdtablesize(), &mask, NULL, NULL, NULL);

	if( ret <= 0 ) { perror("select"); exit(-1);}

/* Atendemos los clientes con datos */
	for(i = 0; i < MAX_CLIENTS; i++) {
	    if(client[i] != -1 && FD_ISSET(client[i], &mask)) {
		if( (cnt=read(client[i], buf, size)) > 0 && 
	    	    write(client[i], buf, cnt) > 0)
			;
		else { /* murio un cliente */
		    fprintf(stderr, "cliente desconectado\n");
		    close(client[i]);
		    client[i] = -1;
		}
	    }
	}

/* aceptamos conexion pendiente si hay una */
	if(FD_ISSET(s, &mask)) {
	    for( i = 0; i < MAX_CLIENTS; i++ )
		if(client[i] == -1) break;
	    if(i == MAX_CLIENTS) {
		fprintf(stderr, "No mas clientes!\n");
		close(j_accept(s)); /* lo rechazo */
		continue;
	    }
	    client[i] = j_accept(s);
	    fprintf(stderr, "cliente conectado\n");
	}
   }
   return 0;
}
