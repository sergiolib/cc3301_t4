#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "jsocket.h"

typedef void *(*Thread_fun)(void *);

#define BUF_SIZE 200

void *reader_fun(long s) {
  int cnt;
  char buf[BUF_SIZE];
  printf("reader started for %ld\n", s);
  while ((cnt= read(s, buf, BUF_SIZE))>0)
    write(1, buf, cnt);

  printf("reader finished\n");
  return NULL;
}

int main(int argc, char **argv) {
  long s;
  int cnt;
  char buf[BUF_SIZE];
  pthread_t reader;
  char *host= argc>=2 ? argv[1] : "localhost";
  int port= argc>=3 ? atoi(argv[2]) : 1818;

  s = j_socket();

  if(j_connect(s, host, port) < 0) {
    fprintf(stderr, "connection refused\n");
    exit(1);
  }

  if (pthread_create(&reader, NULL, (Thread_fun)reader_fun, (void*)s) != 0) {
    fprintf(stderr, "No puede crear thread lector\n");
    exit(1);
  }

  while ((cnt= read(0, buf, BUF_SIZE)) > 0) {
    if (write(s, buf, cnt) != cnt) {
      fprintf(stderr, "Fallo el write al servidor\n");
      break;
    }
  }

  close(s);

  printf("Client finished\n");

  return 0;
}
