#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "jsocket.h"

#define BUF_SIZE 10

int main() {
   int s;
   int cnt, n, size = BUF_SIZE;
   char buf[BUF_SIZE];

   s = j_socket();

   if(j_connect(s, "localhost", 1818) < 0) {
	fprintf(stderr, "connection refused\n");
	exit(1);
   }

   write(s, "hola", 5);
   n = 0;
   while((cnt=read(s, buf+n, size-n)) > 0) {
	n += cnt;
	if(n >= 5) break;
   }
   if(n < 5)
  	printf("fallo el read\n");
   else
   	printf("%s\n", buf);
   close(s);

   return 0;
}
