#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include "jsocket.h"

/*
 * server_chat: servidor de chat con threads
 */

/*
 * Este es el servidor que recibe el socket conectado: s
 */

typedef void *(*Thread_fun)(void *);

#define BUF_SIZE 200
#define N 10

typedef struct {
  char buf[BUF_SIZE];
  int size;
} Message;

pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond= PTHREAD_COND_INITIALIZER;

Message messages[N];
int next_message= 0;

void broadcast(char *buf, int size) {
  Message *msg;
  pthread_mutex_lock(&mutex);
  msg= &messages[next_message%N];
  memcpy(msg->buf, buf, size);
  msg->size= size;
  next_message++;
  pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mutex);
}

int get_message(char *buf, int msg_id) {
  int size;
  Message *msg;
  pthread_mutex_lock(&mutex);
  while (msg_id>=next_message) {
    printf("waiting\n");
    pthread_cond_wait(&cond, &mutex);
  }
  printf("got message\n");
  msg= &messages[msg_id%N];
  size= next_message-msg_id>N ? 0 : msg->size;
  memcpy(buf, msg->buf, size);
  pthread_mutex_unlock(&mutex);
  return size;
} 

void *writer_fun(long s) {
  char buf[BUF_SIZE];
  int curr= next_message;
  for (;;) {
    int cnt= get_message(buf, curr);
    if (cnt>0) {
      printf("writing message %d (%d bytes)\n", curr, cnt);
      if (write(s, buf, cnt)<=0) {
        printf("broken pipe\n");
        return NULL; /* Broken pipe */
      }
    }
    curr++;
  }
}

void *serv(long s) {
  int cnt, size = BUF_SIZE;
  char buf[BUF_SIZE];
  pthread_t writer;

  if (pthread_create(&writer, NULL, (Thread_fun)writer_fun, (void*)s) != 0) {
    fprintf(stderr, "No pude crear un writer\n");
    return NULL;
  }

  fprintf(stderr, "cliente conectado\n");
  while ((cnt= read(s, buf, size)) > 0) {
    fwrite(buf, cnt, 1, stdout);
    broadcast(buf, cnt);
  }

  close(s);
  pthread_join(writer, NULL);
  fprintf(stderr, "cliente desconectado\n");
  return NULL;
}

int main(int argc, char **argv) {
  long s, s2;
  pthread_t pid;
  int port= argc>=2 ? atoi(argv[1]) : 1818;

  signal(SIGPIPE, SIG_IGN);

  s = j_socket();

  if(j_bind(s, port) < 0) {
    fprintf(stderr, "bind failed\n");
    exit(1);
  }

  /* Cada vez que se conecta un cliente le creo un thread */
  for(;;) {
    pthread_attr_t attr;
    s2= j_accept(s);
    pthread_attr_init(&attr);
    if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) != 0) {
      fprintf(stderr, "No se puede establecer el atributo\n");
    }
    if ( pthread_create(&pid, &attr, (Thread_fun)serv, (void *)s2) != 0) {
      fprintf(stderr, "No pude crear thread para nuevo cliente %ld!!!\n", s2);
      close(s2);
    }
    pthread_attr_destroy(&attr);
  }

  return 0;
}
