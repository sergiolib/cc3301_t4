#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include "jsocket.h"

/*
 * server_echo4: servidor multi-clientes con threads
 */

/*
 * Este es el servidor que recibe el socket conectado: s
 */

typedef void *(*Thread_fun)(void *);

#define BUF_SIZE 200

void *serv(long s) {
   int cnt, size = BUF_SIZE;
   char buf[BUF_SIZE];

    fprintf(stderr, "cliente conectado\n");
    while((cnt=read(s, buf, size)) > 0)
	 if(write(s, buf, cnt) < 0) break; /* broken pipe */

   close(s);
   fprintf(stderr, "cliente desconectado\n");
   return NULL;
}

int main() {
   long s, s2;
   pthread_t pid;

   signal(SIGPIPE, SIG_IGN);

   s = j_socket();

   if(j_bind(s, 1818) < 0) {
	fprintf(stderr, "bind failed\n");
	exit(1);
   }

   /* Cada vez que se conecta un cliente le creo un thread */
   for(;;) {
	s2= j_accept(s);
	if( pthread_create(&pid, NULL, (Thread_fun)serv, (void *)s2) != 0) {
	    fprintf(stderr, "No pude crear thread!!!\n");
	    exit(1);
	}
   }
   return 0;
}
